import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

typedef CallHandler = Future<dynamic> Function(MethodCall call);

/// Class to capture screenshots with native code working on background
class NativeScreenshot {
  /// Comunication property to talk to the native background code.
  static final _channel = const MethodChannel('native_screenshot');

  Function onScreenShotSaved;

  NativeScreenshot(this.onScreenShotSaved) {
    _channel.setMethodCallHandler(onMethodCall);
  }

  /// Captures everything as is shown in user's device.
  ///
  /// Returns [null] if an error ocurrs.
  /// Returns a [String] with the path of the screenshot.
  Future<dynamic> takeScreenshot() async {
    if (Platform.isIOS) {
      return await _channel.invokeMethod('takeScreenshot');
    } else {
      return await _channel.invokeMethod('takeScreenshot');
    }
  } // takeScreenshot()

  Future<dynamic> onMethodCall(MethodCall call) async {
    try {
      switch (call.method) {
        case 'SCREENSHOT_SAVED':
          if (onScreenShotSaved != null) {
            onScreenShotSaved(
                call.arguments != null ? call.arguments as String : null);
          }
          break;
      }
    } catch (e) {
      print('Native screenshot callback error: ' + e);
    }
  }
} // NativeScreenshot
