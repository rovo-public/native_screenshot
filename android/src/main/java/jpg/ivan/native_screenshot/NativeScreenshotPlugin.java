package jpg.ivan.native_screenshot;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.Date;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.embedding.engine.renderer.FlutterRenderer;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterView;

/**
 * NativeScreenshotPlugin
 */
public class NativeScreenshotPlugin implements MethodCallHandler, FlutterPlugin, ActivityAware {
    private static final String TAG = "NativeScreenshotPlugin";

    private Context context;
    private MethodChannel channel;
    private Activity activity;
    private Object renderer;

    // Default constructor for old registrar
    public NativeScreenshotPlugin() {
    } // NativeScreenshotPlugin()

    // Condensed logic to initialize the plugin
    private void initPlugin(Context context, BinaryMessenger messenger, Activity activity, Object renderer) {
        this.context = context;
        this.activity = activity;
        this.renderer = renderer;

        this.channel = new MethodChannel(messenger, "native_screenshot");
        this.channel.setMethodCallHandler(this);
    } // initPlugin()

    // New v2 listener methods
    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        this.channel.setMethodCallHandler(null);
        this.channel = null;
        this.context = null;
    } // onDetachedFromEngine()

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        Log.println(Log.INFO, TAG, "Using *NEW* registrar method!");

        initPlugin(
                flutterPluginBinding.getApplicationContext(),
                flutterPluginBinding.getBinaryMessenger(),
                null,
                flutterPluginBinding.getFlutterEngine().getRenderer()
        ); // initPlugin()
    } // onAttachedToEngine()

    // Old v1 register method
    // FIX: Make instance variables set with the old method
    public static void registerWith(Registrar registrar) {
        Log.println(Log.INFO, TAG, "Using *OLD* registrar method!");

        NativeScreenshotPlugin instance = new NativeScreenshotPlugin();

        instance.initPlugin(
                registrar.context(),
                registrar.messenger(),
                registrar.activity(),
                registrar.view()
        ); // initPlugin()
    } // registerWith()


    // Activity condensed methods
    private void attachActivity(ActivityPluginBinding binding) {
        this.activity = binding.getActivity();
    } // attachActivity()

    private void detachActivity() {
        this.activity = null;
    } // attachActivity()


    // Activity listener methods
    @Override
    public void onAttachedToActivity(ActivityPluginBinding binding) {
        attachActivity(binding);
    } // onAttachedToActivity()

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        detachActivity();
    } // onDetachedFromActivityForConfigChanges()

    @Override
    public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
        attachActivity(binding);
    } // onReattachedToActivityForConfigChanges()

    @Override
    public void onDetachedFromActivity() {
        detachActivity();
    } // onDetachedFromActivity()


    // MethodCall, manage stuff coming from Dart
    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (!permissionToWrite()) {
            result.success(null);

            return;
        } // if cannot write

        if (!call.method.equals("takeScreenshot")) {
            result.notImplemented();

            return;
        } // if not implemented


        // Need to fix takeScreenshot()
        // it produces just a black image
        takeScreenshot(result);
    } // onMethodCall()


    // Own functions, plugin specific functionality
    private String getScreenshotName() {
        java.text.SimpleDateFormat sf = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
        String sDate = sf.format(new Date());

        return "native_screenshot-" + sDate + ".png";
    } // getScreenshotName()

    private String getApplicationName() {
        ApplicationInfo appInfo = null;

        try {
            appInfo = this.context.getPackageManager()
                    .getApplicationInfo(this.context.getPackageName(), 0);
        } catch (Exception ex) {
            Log.println(Log.INFO, TAG, "Error getting package name, using default. Err: " + ex.getMessage());
        }

        if (appInfo == null) {
            return "NativeScreenshot";
        } // if null

        CharSequence cs = this.context.getPackageManager().getApplicationLabel(appInfo);
        StringBuilder name = new StringBuilder(cs.length());

        name.append(cs);

        return name.toString();
    } // getApplicationName()

    private String getScreenshotPath() {
        String externalDir = Environment.getExternalStorageDirectory().getAbsolutePath();

        String sDir = externalDir
                + File.separator
                + getApplicationName();

        File dir = new File(sDir);

        String dirPath;

        if (dir.exists() || dir.mkdir()) {
            dirPath = sDir + File.separator + getScreenshotName();
        } else {
            dirPath = externalDir + File.separator + getScreenshotName();
        }

        Log.println(Log.INFO, TAG, "Built Screenshot Path: " + dirPath);

        return dirPath;
    } // getScreenshotPath()

    private void takeScreenshot(Result result) {
        Log.println(Log.INFO, TAG, "Trying to take screenshot [old way] " + System.currentTimeMillis());

        try {
            View view = this.activity.getWindow().getDecorView().getRootView();

            view.setDrawingCacheEnabled(true);

            Bitmap bitmap = null;
            if (this.renderer.getClass() == FlutterView.class) {
                bitmap = ((FlutterView) this.renderer).getBitmap();
            } else if (this.renderer.getClass() == FlutterRenderer.class) {
                bitmap = ((FlutterRenderer) this.renderer).getBitmap();
            }

            if (bitmap == null) {
                Log.println(Log.INFO, TAG, "The bitmap cannot be created :(");
                result.success(false);
                return;
            } // if

            view.setDrawingCacheEnabled(false);

            Log.println(Log.INFO, TAG, "Before writing bitmap " + System.currentTimeMillis());
            result.success(true);
            new StoreScreenShotTask(this.activity, this.channel, TAG, getScreenshotPath(), bitmap).execute();
        } catch (Exception ex) {
            Log.println(Log.INFO, TAG, "Error taking screenshot: " + ex.getMessage());
            result.success(false);
        }
    } // takeScreenshot()

    private boolean permissionToWrite() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Log.println(Log.INFO, TAG, "Permission to write false due to version codes.");

            return true;
        }

        int perm = this.activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (perm == PackageManager.PERMISSION_GRANTED) {
            Log.println(Log.INFO, TAG, "Permission to write granted!");

            return true;
        } // if

        Log.println(Log.INFO, TAG, "Requesting permissions...");
        this.activity.requestPermissions(
                new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                11
        ); // requestPermissions()

        Log.println(Log.INFO, TAG, "No permissions :(");

        return false;
    } // permissionToWrite()

    public static class StoreScreenShotTask extends AsyncTask<Object, Void, String> {
        private String TAG;
        private String imagePath;
        private Bitmap bitmap;
        private WeakReference<Activity> activity;
        private MethodChannel channel;

        StoreScreenShotTask(Activity activity, MethodChannel channel, String tag, String path, Bitmap bitmap) {
            this.TAG = tag;
            this.imagePath = path;
            this.bitmap = bitmap;
            this.activity = new WeakReference<>(activity);
            this.channel = channel;
        }

        protected void onPreExecute() {
            // do nothing here
        }

        protected String doInBackground(Object... params) {
            try {
                File imageFile = new File(imagePath);
                FileOutputStream oStream = new FileOutputStream(imageFile);

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
                oStream.flush();
                oStream.close();

                return imagePath;
            } catch (Exception ex) {
                Log.println(Log.INFO, TAG, "Error writing bitmap: " + ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File file = new File(imagePath);
                Uri uri = Uri.fromFile(file);

                intent.setData(uri);
                this.activity.get().sendBroadcast(intent);
            } catch (Exception ex) {
                Log.println(Log.INFO, TAG, "Error reloading media lib: " + ex.getMessage());
            }

            Log.println(Log.INFO, TAG, "Before return path " + System.currentTimeMillis());
            this.channel.invokeMethod("SCREENSHOT_SAVED", this.imagePath);
        }
    }
} // NativeScreenshotPlugin